﻿global using Furion;

global using Microsoft.AspNetCore.Components;

global using System.Collections.Generic;
global using System.Net.Http;
global using System.Threading.Tasks;

global using ThingsBlazor.Application;
global using ThingsBlazor.Core;
global using ThingsBlazor.Core.Extension;
global using ThingsBlazor.Web.Rcl.Core;
