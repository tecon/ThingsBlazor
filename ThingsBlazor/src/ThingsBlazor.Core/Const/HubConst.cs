﻿namespace ThingsBlazor.Core
{
    public class HubConst
    {
        public const string HubUrl = "/hubs/thingsblazor";
    }
}