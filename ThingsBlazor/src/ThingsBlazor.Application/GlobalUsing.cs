﻿global using Furion;
global using Furion.DependencyInjection;
global using Furion.EventBus;
global using Furion.FriendlyException;

global using Mapster;

global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.SignalR;
global using Microsoft.CodeAnalysis;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Logging;

global using NewLife.Serialization;

global using SqlSugar;

global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Reflection;

global using ThingsBlazor.Core;
global using ThingsBlazor.Core.Extension;

/* 项目“ThingsBlazor.Application (net7.0)”的未合并的更改
在此之前:
global using Yitter.IdGenerator;
在此之后:
global using ThingsBlazor.Core.Utils;
*/
global using ThingsBlazor.Core.Utils;

global using Yitter.IdGenerator;
